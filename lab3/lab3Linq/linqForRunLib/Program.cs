﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using libForLinq;

namespace linqForRunLib // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Create a list of farms
            /*var name = from farm in ModelTests.FarmList
                let percentile = (int)farm.Capacity / 10
                group farm by percentile
                into groups
                where groups.Key >= 2
                orderby groups.Key
                select groups;
            
            // grouping is an IGrouping<int, Country>
            foreach (var grouping in name)
            {
                Console.WriteLine(grouping.Key);
                foreach (var country in grouping)
                {
                    Console.WriteLine(country.Name + ":" + country.Capacity);
                }
            }*/

            /*int[] array = { 1, 2, 3, 4, 5 };

            var newTest =
                from farm in ModelTests.FarmList
                join number in array on farm.tractorId equals number // Assuming tractorId is the key to join on
                select new
                {
                    Farm = farm,
                    Number = number
                };

            foreach (var item in newTest)
            {
                Console.WriteLine($"Farm: {item.Farm.Name}, Tractor ID: {item.Farm.tractorId}, Number: {item.Number}");
            }*/
            
            string[] names = { "Svetlana Omelchenko", "Claire O'Donnell", "Sven Mortensen", "Cesar Garcia" };
            
            IEnumerable<string> queryFirstNames =
                from name in names
                let firstName = name.Split(' ')[0]
                select firstName;

            foreach (var s in queryFirstNames)
            {
                Console.Write(s + " ");
            }
            
            
        }
    }
}
