using System.Collections;
using System.Xml.Linq;
using libForLinq;

namespace UnitTestLinq;

public class Tests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Test1()
    {
        Assert.Pass();
    }


    [Test]
    public void Test2()
    {
        var sortedCharacters = ModelTests.FarmList.OrderBy(u => u.Name).First().Name;

        var expect = "Alice";

        Assert.Pass(sortedCharacters, expect);
    }

    [Test]
    public void TestQueqe()
    {
        var list_quque = new Queue<Farm>(ModelTests.FarmList);

        Assert.AreEqual(list_quque.Peek().Name, "John");
    }

    [Test]
    public void TestStack()
    {
        var list_stack = new Stack<Farm>(ModelTests.FarmList);


        var expected_count = 0;

        while (list_stack.Count > 0) list_stack.Pop();

        Assert.AreEqual(list_stack.Count, expected_count);
    }


    [Test]
    public void TestHash()
    {
        var list_hash = new HashSet<Farm>(ModelTests.FarmList);

        Assert.AreEqual(list_hash.Count, ModelTests.FarmList.Count);
    }

    [Test]
    public void TestSortedListExtension()
    {
        
    }
///////////////////////////////
    /// <summary>
    /// 6) group by the name
    /// </summary>
    [Test]
    public void TestGroupByTractorId()
    {
        var listTest = ModelTests.FarmList.OrderBy(u => u.Name);

        // Now you can use the ToDictionary method here if needed
        var dictionary = listTest.ToDictionary(item => item.Name, item => item);

        string[] expects = { "Alice", "Bob", "David", "Emma", "John", "Sophia" };

        foreach (var expect in expects)
            Assert.Contains(expect, dictionary.Keys);
    }


    [Test]
    public void TestGroupByTDesengnidId()
    {
        var list_test = ModelTests.FarmList.OrderBy(u => u.Name);

        // Now you can use the ToDictionary method here if needed
        var dictionary = list_test.ToDictionary(item => item.Name, item => item); 

        string[] expects = { "Alice", "Bob", "David", "Emma", "John", "Sophia" };

        // Check if the dictionary keys are in the same order as the expects array
        var index = 0;
        foreach (var key in dictionary.Keys)
        {
            // Ensure we don't go out of bounds of the expects array
            if (index >= expects.Length) Assert.Fail("Dictionary contains more keys than expected.");

            // Compare the current key with the corresponding value in expects array
            Assert.AreEqual(expects[index], key);

            // Move to the next index
            index++;
        }

        // Check if the expects array contains all keys from the dictionary
        Assert.AreEqual(expects.Length, dictionary.Count);
    }


    /// <summary>
    /// For check if the test coorect sort in decending by the key
    /// </summary>
    [Test]
    public void TestDescendingOrder()
    {
        var dictionary = (from item in ModelTests.FarmList
                select new { item.Name, Item = item })
            .ToDictionary(x => x.Name, x => x.Item);

        
        
        var orderedDictionary = (from kv in dictionary
                orderby kv.Key descending
                select kv)
            .ToDictionary(kv => kv.Key, kv => kv.Value);


        Assert.AreEqual(dictionary.Count, orderedDictionary.Count);

        foreach (var kvp in dictionary)
        {
            Assert.IsTrue(orderedDictionary.ContainsKey(kvp.Key));
            Assert.AreEqual(kvp.Value, orderedDictionary[kvp.Key]);
        }
    }
//

    /// <summary>
    /// in this Test i wi can check how to connect tow dictionary
    /// and check if present element from the first and second in the combined
    /// </summary>
    [Test]
    public void TestJoinForTwoLists()
    {
        var dictionary1 = ModelTests.FarmList.ToDictionary(item => item.Name, item => item);
        var dictionary2 = ModelTests.FarmListOfEnemy.ToDictionary(item => item.Name, item => item);

        var combinedDictionary = dictionary1.Concat(dictionary2)
            .ToDictionary(kv => kv.Key, kv => kv.Value);

        Assert.AreEqual(ModelTests.FarmList.Count + ModelTests.FarmListOfEnemy.Count, combinedDictionary.Count);

        foreach (var kvp in dictionary1)
        {
            Assert.IsTrue(combinedDictionary.ContainsKey(kvp.Key));
            Assert.AreEqual(kvp.Value, combinedDictionary[kvp.Key]);
        }

        foreach (var kvp in dictionary2)
        {
            Assert.IsTrue(combinedDictionary.ContainsKey(kvp.Key));
            Assert.AreEqual(kvp.Value, combinedDictionary[kvp.Key]);
        }
    }

    [Test]
    public void TestFilter()
    {
        var dictionary1 = ModelTests.FarmList.ToDictionary(g => g.Name);

        string[] expectNames = { "John", "Alice", "Bob", "Emma", "David", "Sophia" };

        foreach (var expectName in expectNames)
        {
            Assert.Contains(expectName, dictionary1.Select(kv => kv.Value.Name).ToList());
        }
    }


    /// <summary>
    /// This code is for 7 mission for sorting person
    /// farm that have only more than 50 litres of bans
    /// </summary>
    [Test]
    public void TestForGroupMore50()
    {
        var sorted_list = ModelTests.FarmList
            .Join(ModelTests.tractorListN,
                ferm => ferm.tractorId,
                tractor => tractor.ID,
                (ferm, tractor) => new { Ferm = ferm, Tractor = tractor })
            .Where(item => item.Tractor.litres > 50)
            .GroupBy(item => item.Tractor.ID, item => item.Ferm)
            .SelectMany(group => group.ToList())
            .ToList();
        

        var expects = new[] { "John", "Emma", "Bob", "Sophia" };

        foreach (var expect in expects)
        {
            // Check if the name exits in the sorted_list
            Assert.IsTrue(sorted_list.Any(item => item.Name == expect));
        }
    }


    /// <summary>
    /// This test is going for misson working with dictionary and lists
    /// </summary>
    [Test]
    public void WorkWithListAndDictionary()
    {
        var listOfFarm = new List<Farm>(ModelTests.FarmList);
        var dictionaryOfFarm = listOfFarm.ToDictionary(u => u.Name, u => u);


        var findCapacityInList = listOfFarm.Where(u => u.Capacity > 80);

        var findCapacityInDictionary = dictionaryOfFarm
            .Where(u => u.Value.Capacity < 85);


        Assert.AreEqual(findCapacityInDictionary.Count(), 2);
        Assert.AreEqual(findCapacityInList.Count(), 4);


        //delete the person with the key
        var didDeleteThePerson = findCapacityInDictionary.Where(u => u.Key != "Sophia")
            .ToDictionary(u => u.Key,
                u => u.Value);

        Assert.AreEqual(didDeleteThePerson.Count(), 1);
    }


    
    /// <summary>
    /// Thing for test anonimous class and tasl
    /// </summary>
    [Test]
    public void TestWithAnonimusClass()
    {
        var Farm = new
        {
            ID = 1,
            litres = 100,
            sitPlace = 20
        };
       
        
        Assert.Pass();
    }


    /// <summary>
    /// This test i created for check such a thing like Compare
    /// in which i campare two class.capacity
    /// </summary>
    [Test]
    public void TestMyComparer()
    {
        var listOfFarm = ModelTests.FarmList;
        int[] expectName = { 75, 80, 85, 90, 95, 100 };

        MyComperer comparer = new MyComperer();

        listOfFarm.Sort(comparer);

        for (int i = 0; i < listOfFarm.Count; i++)
        {
            Assert.AreEqual(listOfFarm[i].Capacity, expectName[i]);
        }
    }
    
    
    /// <summary>
    /// This is for convert the list into the array
    /// </summary>
    [Test]
    public void TestConvertingMassiveIntoList()
    {
        var list = new List<Farm>(ModelTests.FarmList);

        var array = list.ToArray();

        Assert.AreEqual(list, array);
    }
    
    
}
