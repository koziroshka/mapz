﻿using System.Security.Cryptography;

namespace libForLinq
{
    /// <summary>
    /// Class that link with class "Farm"
    /// </summary>
    public class Tractor
    {
        public int litres { get; set; }
        public int sitPlace { get; set; }
        public int ID { get; set; }
    }

    
    /// <summary>
    /// Class that is based for class tractror
    /// </summary>
    public class Farm
    {
        public Guid fermId { get; set; }
        public string Name { get; set; }
        public int Capacity { get; set; }
        public int tractorId { get; set; }

        public Farm(string name, int capacity, int id)
        {
            Name = name;
            Capacity = capacity;
            tractorId = id;
        }

        public Farm(Guid fermId, string name, int capacity, int id)
        {
            this.fermId = fermId;
            Name = name;
            Capacity = capacity;
            tractorId = id;
        }
    }

    
    
    /// <summary>
    /// On this ModalTests i will create tests for my lab number 3
    /// </summary>
    public static class ModelTests
    {
        public static List<Tractor> tractorListN
        {
            get
            {
                return new List<Tractor>()
                {
                    new Tractor() { litres = 100, sitPlace = 200, ID = 1 },
                    new Tractor() { litres = 20, sitPlace = 100, ID = 2 },
                    new Tractor() { litres = 51, sitPlace = 20, ID = 3 }
                };
            }
        }

        public static List<Farm> FarmList
        {
            get
            {
                return new List<Farm>()
                {
                    new Farm(new Guid(), "John", 100, 1),
                    new Farm(new Guid(), "Alice", 90, 2),
                    new Farm(new Guid(), "Emma", 95, 1),
                    new Farm(new Guid(), "Bob", 80, 3),
                    new Farm(new Guid(), "David", 85, 2),
                    new Farm(new Guid(), "Sophia", 75, 3),
                };
            }
        }

        public static List<Farm> FarmListOfEnemy
        {
            get
            {
                return new List<Farm>()
                {
                    new Farm(new Guid(), "Stupid", 85, 2),
                    new Farm(new Guid(), "Bitch", 75, 3),
                };
            }
        }


        public static Dictionary<Guid, TSource> ToDictionary<TSource>(
            this IEnumerable<TSource> source,
            Func<TSource, Guid> keySelector,
            Func<TSource, TSource> elementSelector)
        {
            var dictionary = new Dictionary<Guid, TSource>();

            foreach (var item in source)
            {
                var key = keySelector(item);
                var element = elementSelector(item);

                dictionary[key] = element;
            }

            return dictionary;
        }
    }


    public class MyComperer : IComparer<Farm>
    {
        public int Compare(Farm x, Farm y)
        {
            return x.Capacity.CompareTo(y.Capacity);
            
        }
    }
    
}
