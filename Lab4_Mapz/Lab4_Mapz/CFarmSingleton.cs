﻿using System;

namespace Lab4_Mapz
{
    public interface FarmHealth
    {
        
        int AmountOfEat { get; set; }
    }
    
    /*public sealed class Map : FarmHealth
    {
        private static Map instance;
        private int[,] grid;
        
        
        private int availableCells;
        private Map()
        {
            // Initialize the grid and other necessary data
            grid = new int[10, 10];
            availableCells = 100; // Припустимо, що початкова кількість доступних ділянок - 100
        }

        // Method to get the singleton instance of the Map class
        public static Map GetInstance()
        {
            if (instance == null)
            {
                instance = new Map();
            }
            return instance;
        }

        // Additional methods for weather conditions
        public void Raining()
        {
            // Logic for when it's raining
            Console.WriteLine("It's raining on the map.");
        }

        public void Shining()
        {
            // Logic for when it's shining
            Console.WriteLine("The sun is shining on the map.");
        }

        public int AmountOfEat { get; set; }
    }*/
    
    
    /// <summary>
    /// This implementation is thread-safe. The thread takes out a lock on a shared object,
    /// and then checks whether or not the instance has been created before creating the instance.
    /// This takes care of the memory barrier issue (as locking makes sure that all reads occur
    /// logically after the lock acquire, and unlocking makes sure that all
    /// writes occur logically before the lock release)
    /// and ensures that only one thread will create an instance (as only one 
    ///thread can be in that part of the code at a time
    /// - by the time the second thread enters it,the first thread will have
    /// created the instance, so the expression will evaluate to false).
    /// Unfortunately, performance suffers as a lock is acquired every time the instance is requested.
    /// </summary>
    public sealed class Map : FarmHealth
    {
        private static Map instance = null;
        private static readonly object padlock = new object();
        private int[,] grid;
        private int _availableCells;
        private int speedOfGrowingGrase;
        
        Map()
        {
            this.speedOfGrowingGrase = 2;
            _availableCells = 100;
            grid = new int[10, 10];
        }

        public static Map Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Map();
                    }
                    return instance;
                }
            }
        }

        public void Raining()
        {
            // Logic for when it's raining
            Console.WriteLine(@"It's raining on the map.");
            speedOfGrowingGrase += 1;
        }

        public void Shining()
        {
            // Logic for when it's shining
            Console.WriteLine(@"The sun is shining on the map.");
        }
        
        
        public int AmountOfEat { get; set; }
    }

}
