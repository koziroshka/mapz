﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Runtime.InteropServices;

namespace lab02_version_001
{
 public sealed class gg
 {
  
 }

 class Program
 {

  static void Main(string[] args)
  {

   /*1-2------
   FishFarm fishFarm = new FishFarm("My Fish Farm", 100, 2.5);


   // Виклик методів та властивостей класу FishFarm
   Console.WriteLine($"Farm Name: {fishFarm.Name}");
   Console.WriteLine($"Description: {fishFarm.Description}");
   Console.WriteLine($"Capacity: {fishFarm.Capacity}");
   Console.WriteLine($"Income: {fishFarm.CalculateIncome()}");

   // Виклик методу Interact, реалізованого через інтерфейс
   fishFarm.Interact();

   //іfishFarm.farmName = "New Name"; // Помилка компіляції: поле farmName є protected і недоступне ззовні класу Farm*/

   /*1-3-----fieldForRelax myClass = new fieldForRelax();
   // Звернення до полів та методів класу MyClass
   // myClass.myPrivateField; // Помилка компіляції: поле myPrivateField є private і недоступне ззовні класу MyClass
   // myClass.MyPrivateMethod(); // Помилка компіляції: метод MyPrivateMethod() є private і недоступний ззовні класу MyClass


   //fieldForRelax.Threes = new fieldForRelax.Threes();

   /*SFieldForRelax name = new SFieldForRelax();
   name.My
   IField myInterface = null;
   myInterface.MyInterfaceMethod();*/

   /*1-4----
   FishFarm fishFarm = new FishFarm("My Fish Farm", 100, 2.5);
   fishFarm.SicretRoomOfFermer*/

   /*1-5-----
    KindOfFish fish1 = KindOfFish.Salmon;
   KindOfFish fish2 = KindOfFish.Tuna;

   bool areEqual = fish1 == fish2;
   Console.WriteLine($"Are fish1 and fish2 equal? {areEqual}");

   bool isCod = fish1 == KindOfFish.Cod;
   Console.WriteLine($"Is fish1 a Cod? {isCod}");

   bool isSalmonOrTuna = fish1 == KindOfFish.Salmon || fish2 == KindOfFish.Tuna;
   Console.WriteLine($"Is fish1 a Salmon or fish2 a Tuna? {isSalmonOrTuna}");

   bool isNotMackerel = fish1 != KindOfFish.Mackerel && fish2 != KindOfFish.Mackerel;
   Console.WriteLine($"Are neither fish1 nor fish2 Mackerel? {isNotMackerel}");

   bool bothSalmon = (fish1 & fish2) == KindOfFish.Salmon;
   Console.WriteLine($"Are both fish1 and fish2 Salmon? {bothSalmon}");*/

   /*1-7----------
    FishFarm fishFarm1 = new FishFarm("Fish Farm 1", 100, 2.5);
    Console.WriteLine("Details of Fish Farm 1:");
    fishFarm1.PrintDetails();
    Console.WriteLine();

    // Виклик другого конструктора класу FishFarm
    FishFarm fishFarm2 = new FishFarm("Fish Farm 2", 50);
    Console.WriteLine("Details of Fish Farm 2:");
    fishFarm2.PrintDetails();*/


   /*1-8-----
    Console.WriteLine("Creating object 1:");
    MyClassForTest1_8 obj1 = new MyClassForTest1_8();
    obj1.PrintFields();
    Console.WriteLine();

    Console.WriteLine("Creating object 2:");
    MyClassForTest1_8 obj2 = new MyClassForTest1_8();
    obj2.PrintFields();*/



   /* 1-9-------
     // Виклик методу IncreaseAmountOfFood без параметра ref
    FishFarm fishFarm = new FishFarm();
     Console.WriteLine($"Initial Food: {initialFood}");
     fishFarm.IncreaseAmountOfFood(ref initialFood);
     Console.WriteLine($"Food after IncreaseAmountOfFood: {initialFood}");
     Console.WriteLine();

     // Виклик методу InitializeValue без параметра out
     Console.WriteLine($"Initial value: {value}");
     fishFarm.InitializeValue(out value);
     Console.WriteLine($"Value initialized using InitializeValue: {value}");*/


   /*Start of 1_10
    FishFarm testFor1_10 = new FishFarm("Kozak", 2, 150);
    object obj = testFor1_10;

    Console.WriteLine(obj.GetType());

    // Unboxing: об'єкт знову змінюється в int
    if (obj is FishFarm)
    {
        FishFarm unboxedNumber = (FishFarm)obj; // Unboxing
        Console.WriteLine("Unboxed value: " + unboxedNumber);
    }
    else
    {
        Console.WriteLine("Object cannot be unboxed to FishFarm type.");
    }
    End of 1-10*/

   /* 1-11-----
    int amountOfFish = 10;

    FishFarm explicitOperator = amountOfFish;

    explicitOperator.PrintDetails();*/

   
   /*1-14---
    MyObject obj = new MyObject();

   // Виклик перевизначених методів
   Console.WriteLine(obj.ToString());
   Console.WriteLine(obj.GetHashCode());
   Console.WriteLine(obj.Equals(new MyObject()));
   Console.WriteLine(obj.GetHashCode(2));*/
   
   MyStruct myStruct = new MyStruct();
   myStruct.MyMethod();
  }
 }
}
/*1-2------
           FishFarm fishFarm = new FishFarm("My Fish Farm", 100, 2.5);


           // Виклик методів та властивостей класу FishFarm
           Console.WriteLine($"Farm Name: {fishFarm.Name}");
           Console.WriteLine($"Description: {fishFarm.Description}");
           Console.WriteLine($"Capacity: {fishFarm.Capacity}");
           Console.WriteLine($"Income: {fishFarm.CalculateIncome()}");

           // Виклик методу Interact, реалізованого через інтерфейс
           fishFarm.Interact();

           //іfishFarm.farmName = "New Name"; // Помилка компіляції: поле farmName є protected і недоступне ззовні класу Farm*/

            /*1-3-----fieldForRelax myClass = new fieldForRelax();
            // Звернення до полів та методів класу MyClass
            // myClass.myPrivateField; // Помилка компіляції: поле myPrivateField є private і недоступне ззовні класу MyClass
            // myClass.MyPrivateMethod(); // Помилка компіляції: метод MyPrivateMethod() є private і недоступний ззовні класу MyClass


            //fieldForRelax.Threes = new fieldForRelax.Threes();

            /*SFieldForRelax name = new SFieldForRelax();
            name.My
            IField myInterface = null;
            myInterface.MyInterfaceMethod();*/

            /*1-4----
            FishFarm fishFarm = new FishFarm("My Fish Farm", 100, 2.5);
            fishFarm.SicretRoomOfFermer*/

            /*1-5-----
             KindOfFish fish1 = KindOfFish.Salmon;
            KindOfFish fish2 = KindOfFish.Tuna;

            bool areEqual = fish1 == fish2;
            Console.WriteLine($"Are fish1 and fish2 equal? {areEqual}");

            bool isCod = fish1 == KindOfFish.Cod;
            Console.WriteLine($"Is fish1 a Cod? {isCod}");

            bool isSalmonOrTuna = fish1 == KindOfFish.Salmon || fish2 == KindOfFish.Tuna;
            Console.WriteLine($"Is fish1 a Salmon or fish2 a Tuna? {isSalmonOrTuna}");

            bool isNotMackerel = fish1 != KindOfFish.Mackerel && fish2 != KindOfFish.Mackerel;
            Console.WriteLine($"Are neither fish1 nor fish2 Mackerel? {isNotMackerel}");

            bool bothSalmon = (fish1 & fish2) == KindOfFish.Salmon;
            Console.WriteLine($"Are both fish1 and fish2 Salmon? {bothSalmon}");*/

            /*1-7----------
             FishFarm fishFarm1 = new FishFarm("Fish Farm 1", 100, 2.5);
             Console.WriteLine("Details of Fish Farm 1:");
             fishFarm1.PrintDetails();
             Console.WriteLine();

             // Виклик другого конструктора класу FishFarm
             FishFarm fishFarm2 = new FishFarm("Fish Farm 2", 50);
             Console.WriteLine("Details of Fish Farm 2:");
             fishFarm2.PrintDetails();*/


            /*1-8-----
             Console.WriteLine("Creating object 1:");
             MyClassForTest1_8 obj1 = new MyClassForTest1_8();
             obj1.PrintFields();
             Console.WriteLine();

             Console.WriteLine("Creating object 2:");
             MyClassForTest1_8 obj2 = new MyClassForTest1_8();
             obj2.PrintFields();*/



            /* 1-9-------
              // Виклик методу IncreaseAmountOfFood без параметра ref
             FishFarm fishFarm = new FishFarm();
              Console.WriteLine($"Initial Food: {initialFood}");
              fishFarm.IncreaseAmountOfFood(ref initialFood);
              Console.WriteLine($"Food after IncreaseAmountOfFood: {initialFood}");
              Console.WriteLine();

              // Виклик методу InitializeValue без параметра out
              Console.WriteLine($"Initial value: {value}");
              fishFarm.InitializeValue(out value);
              Console.WriteLine($"Value initialized using InitializeValue: {value}");*/


            /*Start of 1_10
             FishFarm testFor1_10 = new FishFarm("Kozak", 2, 150);
             object obj = testFor1_10;

             Console.WriteLine(obj.GetType());

             // Unboxing: об'єкт знову змінюється в int
             if (obj is FishFarm)
             {
                 FishFarm unboxedNumber = (FishFarm)obj; // Unboxing
                 Console.WriteLine("Unboxed value: " + unboxedNumber);
             }
             else
             {
                 Console.WriteLine("Object cannot be unboxed to FishFarm type.");
             }
             End of 1-10*/

            /* 1-11-----
             int amountOfFish = 10;

             FishFarm explicitOperator = amountOfFish;

             explicitOperator.PrintDetails();*/
            