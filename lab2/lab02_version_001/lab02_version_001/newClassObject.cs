﻿namespace lab02_version_001;

public class MyObject : Object  {
    public override string ToString()  {
        return "It's my object";
    }

    
    public override int GetHashCode()  {
        return 42;
    }
    public override bool Equals(object obj)  {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        MyObject other = (MyObject)obj;
        return true;
    }

    
    // Перевизначення захищеного методу MemberwiseClone()
    protected object MemberwiseClone()  {
        return base.MemberwiseClone();
    }
    
    // Перевизначення методу GetHashCode() з параметром
    public  int GetHashCode(int multiplier)  {
        return base.GetHashCode() * multiplier; 
    }

    
    // Перевизначення захищеного методу Finalize() (деструктора)
    ~MyObject()  {
        Console.WriteLine("I wanna be free");
    }
    
}

