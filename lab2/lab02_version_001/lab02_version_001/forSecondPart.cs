﻿using System.Drawing;

namespace lab02_version_001;

public class forSecondPart
{
    
}

public class BaseClass
{
    public virtual void DoSomething()
    {
        Console.WriteLine("BaseClass.DoSomething()");
    }
}

// Клас-нащадок 1
public class InheritedClass1 : BaseClass
{
    public override void DoSomething()
    {
        Console.WriteLine("InheritedClass1.DoSomething()");
    }
}

// Клас-нащадок 2
public class InheritedClass2 : BaseClass
{
    public override void DoSomething()
    {
        Console.WriteLine("InheritedClass2.DoSomething()");
    }
}

public class timeCreatign10mClass
{
    private int time;
    private int name;

    public timeCreatign10mClass(int name, int time)
    {
        
        this.name = name;
        this.time = time;
    }

    public List<PointF> points;
}

public struct timeCreatign10mStruct
{
     private int time;
     private int name;

    public timeCreatign10mStruct(int name, int time)
    {
        this.name = name;
        this.time = time;
    }

    public List<PointF> points;
}


public class timerCreating10mInherit : timeCreatign10mClass
{
    public timerCreating10mInherit(int name, int time) : base(name, time)
    {
        
    }
}