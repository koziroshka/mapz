﻿namespace lab02_version_001;



/// <summary>
/// /////////////////////////
/// </summary>

public class BaseClassReflection
{
    protected BaseClassReflection()
    {
        Console.WriteLine("Base class constructor");
    }

    public virtual void DoSmth()
    {
        Console.WriteLine("From baseclass [Reflection]");
    }
}

public class InheritedClassReflection : BaseClassReflection
{
    public InheritedClassReflection()
    {
        Console.WriteLine("Inherited class constructor");
    }

    public override void DoSmth()
    {
        Console.WriteLine("From inherited class [Reflection]");
    }
} 