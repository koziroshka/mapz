﻿namespace lab02_version_001;

public abstract class Farm
{
    protected string farmName; // доступне для успадкованих класів та класів у тому ж самому пакеті
    protected int numberOfAnimals;

    public Farm(string name, int numAnimals)
    {
        farmName = name;
        numberOfAnimals = numAnimals;
    }

    public Farm(string name)
    {
        farmName = name;
    }

    public Farm()
    {
        farmName = "none";
        numberOfAnimals = 0;
    }

    public abstract double CalculateIncome();

    public abstract void IncreaseAmountOfFood(ref int food);

    public abstract int InitializeValue(out int x);
}

    public enum KindOfFish
    {
        Salmon,
        Tuna,
        Cod,
        Trout,
        Bass,
        Sardine,
        Mackerel,
        Haddock,
        MahiMahi,
        Flounder
    }


public interface FarmObject
{
    string Name { get; }
    string Description { get; }

    void Interact();
}

public interface IBarn : FarmObject
{
    int Capacity { get; } // властивість доступна через інтерфейс
}

public class FishFarm : Farm, IBarn
{
    private double bloodworms;
    
    protected class InnerFish // доступний тільки у межах класу FishFarm
    {
        public string Type { get; set; }
    }

    protected class SicretRoomOfFermer
    {
        public int sicretPublic;
        private int sicretPrivate;
        public int sicterPublic;

        public int SicretPublicMethod()
        {
            return 0;
        }

        private int SicretPrivateMethod()
        {
            return 1;
        }
        
        private int SicretProtectMethod()
        {
            return 1;
        }
        
    }
    public FishFarm(string name, int fish, double priceForFood) : base(name, fish)
    {
        bloodworms = priceForFood;
        this.fish = new InnerFish();
    }

    
    public FishFarm(string name, int fish) : base(name)
    {
        bloodworms = fish * 0.5;
        this.fish = new InnerFish();
    }


    public FishFarm() : base()
    {
        bloodworms = 0;
        Capacity = 0;
        Name = null;
        
    }

    public static implicit operator FishFarm(int value)
    {
        FishFarm myFishFarm = new FishFarm("Explicit operator", value, 0);
        return myFishFarm;
    }
    
    
    
    // Методи, які успадковані від класу Farm
    public override double CalculateIncome()
    {
        return numberOfAnimals * bloodworms;
    }

    public override void IncreaseAmountOfFood(ref int food)
    {
        food *= 2;
    }

    public override int InitializeValue(out int x)
    {
        int coeficient = 2;
        x = coeficient;
        return x;
    }
    
    // Властивості та методи інтерфейсу IBarn
    public int Capacity
    {
        get { return numberOfAnimals; }
        set => numberOfAnimals = value;
    }

    public string Name
    {
        get { return farmName; }
        set => farmName = value;
    }

    public string Description { get { return "Fish Farm"; } }

    public void Interact()
    {
        Console.WriteLine($"You are interacting with the FishFarm named {farmName}.");
    }

    private InnerFish fish; // доступний тільки у межах класу FishFarm

    public void PrintDetails()
    {
        Console.WriteLine($"the slarks :{bloodworms}");
        Console.WriteLine($"capecity: {Capacity}");
        Console.WriteLine($"Discription: {Description}");
        Console.WriteLine($"Number of animals: {numberOfAnimals}");
        Console.WriteLine($"Farm name: {farmName}");
    }
}


// Базовий клас
    class fieldForRelax
    {
        int privateField;

         void PrivateMethod()
        {
            Console.WriteLine("This is a private method in the BaseClass.");
        }

        // Вкладений клас
        class Threes
        {
             int AmountOfThree;

             void NestedPrivateMethodAboutThree()
            {
                Console.WriteLine("This is a private method in the NestedClass.");
                
            }
        }
    }

struct SFieldForRelax
{
    int SprivateField;

    void MyPrivateMethodSAppelThree()
    {
        Console.WriteLine("This is a private method in a struct.");
    }
}

interface IField
{
    void MyInterfaceMethod();
}

    
        public class MyClassForTest1_8
        {
            // Статичне поле
            public static string StaticField = "Static field";

            // Динамічне поле
            public string DynamicField;

            // Статичний конструктор
            static MyClassForTest1_8()
            {
                Console.WriteLine("Static constructor called.");
            }

            // Динамічний конструктор
            public MyClassForTest1_8()
            {
                Console.WriteLine("Instance constructor called.");
                DynamicField = "Dynamic field";
            }

            // Метод для виведення значень полів
            public void PrintFields()
            {
                Console.WriteLine("StaticField: " + StaticField);
                Console.WriteLine("DynamicField: " + DynamicField);
            }
        }
    
public interface IMyInterface
{
    void MyMethod();
}

public struct MyStruct : IMyInterface
{
    public void MyMethod()
    {
        Console.WriteLine("Method in struct Konstantin");
    }
}